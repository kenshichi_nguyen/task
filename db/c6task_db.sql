-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2014 at 06:54 PM
-- Server version: 5.5.40
-- PHP Version: 5.4.4-14+deb7u14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c6task_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `cell_id` int(11) NOT NULL,
  `text` text,
  `focus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `row_id`, `cell_id`, `text`, `focus`) VALUES
(1, 1, 1, 'Trò chơi?', 0),
(7, 1, 2, 'Gamesmen', 0),
(8, 2, 1, 'Phó bản?', 0),
(9, 2, 2, '- Active Redis on Live Site => cần thống nhất thời gian active\n- Errors appear when active vanish on Live Site => check lại trên staging site\n- There was a problem with reindexing process =>\nCannot add or update a child row: a foreign key constraint fails\n- Preparing CR', 0),
(10, 1, 3, 'Eway 2,3', 0),
(11, 2, 3, 'done', 0),
(26, 3, 1, 'Ai quẩy?', 0),
(27, 3, 2, 'HoangDV', 0),
(28, 3, 3, 'NghiaHC', 0),
(32, 1, 4, 'Joshs Frogs', 0),
(33, 2, 4, '- Frog error\nhttps://docs.google.com/a/smartosc.com/document/d/1fzXMn96NygNbZ5qQmil7kO1LIIMcqbZ7cjl2PELzO-0/edit\n\nCode on footer --> Mười => done\nAuthorize.net --> Hoàng => done\nM2E error log --> Hoàng => Sent to sale\nMailchimp 461 --> Mười > done', 0),
(34, 3, 4, 'HoangDv, MuoiDV', 0),
(35, 1, 5, 'Facial Co', 0),
(36, 2, 5, 'done', 0),
(37, 3, 5, 'ThaoND', 0),
(38, 1, 6, 'Task', 0),
(39, 2, 6, '- Filter by using only JS to show suitable rows\n- Autoload content from server by Ajax (OK 10s)\n- Detect structure changes from server, reload, remember cell which user is focusing after reloading\n- Nếu save cell bị người dùng khác xóa, thông báo lỗi.\n- Nếu nhiều người dùng thao tác trên 1 cell, tìm cách xử lý, xem google => thông báo thao tác trên một ô không tồn tại (hoặc disable text input)\n- Auto save each 10s for the focusing cell (beside blur cell) with checking content changing  for focusing cell (ok)\n- Phân quyền xoá dòng, cột\n- Hiển thị tên người đang login / sửa\n- Đang hiển thị thanh cuộn ngang => thu nhỏ độ rộng cột để ẩn thanh cuộn ngang', 0),
(40, 3, 6, 'ThaoND, MuoiDV, HoangDV', 0),
(41, 1, 7, 'Suối mỡ 1/11', 0),
(42, 2, 7, '- Thời gian: 1/11 - 2/11\n- Địa điểm: Suối Mỡ, đi rừng, leo núi, bới đá, tìm vàng,..đôi khi nhặt được răng :3\n\nchuẩn bị đồ đạc', 0),
(43, 3, 7, 'ThaoND', 0),
(55, 1, 8, 'Yours', 0),
(56, 2, 8, 'url rewrite magento\nyoursphp', 0),
(57, 3, 8, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(511) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'vinhnv', 'd25e99e457c1e61ad584a8b45f2936b900ef257c'),
(2, 'thaond', 'd72fe8f33e18ae3812eb8c25242cd3105975b359'),
(3, 'hoangdv', 'c64145c910d5808c5540ad79daafd1e50e0f4950'),
(4, 'nghiahc', 'e4fd731fc02285fcbdf9da5e0172d01912cd943e'),
(5, 'muoidv', '03bd085e1e27bb94bfa1278673f792007a8fee39');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
