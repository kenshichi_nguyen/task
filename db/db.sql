/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50157
Source Host           : localhost:3306
Source Database       : tasks

Target Server Type    : MYSQL
Target Server Version : 50157
File Encoding         : 65001

Date: 2014-10-17 09:20:46
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `content`
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` int(11) NOT NULL,
  `cell_id` int(11) NOT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO content VALUES ('1', '1', '1', '');
INSERT INTO content VALUES ('2', '2', '1', '');
INSERT INTO content VALUES ('3', '1', '2', '');
INSERT INTO content VALUES ('4', '2', '2', '');
INSERT INTO content VALUES ('5', '3', '1', '');
INSERT INTO content VALUES ('6', '3', '2', '');
INSERT INTO content VALUES ('7', '1', '3', '');
INSERT INTO content VALUES ('8', '2', '3', '');
INSERT INTO content VALUES ('9', '3', '3', '');
INSERT INTO content VALUES ('10', '4', '1', '');
INSERT INTO content VALUES ('11', '4', '2', '');
INSERT INTO content VALUES ('12', '4', '3', '');
INSERT INTO content VALUES ('13', '1', '4', '');
INSERT INTO content VALUES ('14', '2', '4', '');
INSERT INTO content VALUES ('15', '3', '4', '');
INSERT INTO content VALUES ('16', '4', '4', '');
INSERT INTO content VALUES ('17', '1', '5', '');
INSERT INTO content VALUES ('18', '2', '5', '');
INSERT INTO content VALUES ('19', '3', '5', '');
INSERT INTO content VALUES ('20', '4', '5', '');
INSERT INTO content VALUES ('21', '1', '6', '');
INSERT INTO content VALUES ('22', '2', '6', '');
INSERT INTO content VALUES ('23', '3', '6', '');
INSERT INTO content VALUES ('24', '4', '6', '');
INSERT INTO content VALUES ('25', '1', '7', '');
INSERT INTO content VALUES ('26', '2', '7', '');
INSERT INTO content VALUES ('27', '3', '7', '');
INSERT INTO content VALUES ('28', '4', '7', '');
INSERT INTO content VALUES ('29', '1', '8', '');
INSERT INTO content VALUES ('30', '2', '8', '');
INSERT INTO content VALUES ('31', '3', '8', '');
INSERT INTO content VALUES ('32', '4', '8', '');
INSERT INTO content VALUES ('33', '1', '9', '');
INSERT INTO content VALUES ('34', '2', '9', '');
INSERT INTO content VALUES ('35', '3', '9', '');
INSERT INTO content VALUES ('36', '4', '9', '');
INSERT INTO content VALUES ('37', '5', '1', '');
INSERT INTO content VALUES ('38', '5', '2', '');
INSERT INTO content VALUES ('39', '5', '3', '');
INSERT INTO content VALUES ('40', '5', '4', '');
INSERT INTO content VALUES ('41', '5', '5', '');
INSERT INTO content VALUES ('42', '5', '6', '');
INSERT INTO content VALUES ('43', '5', '7', '');
INSERT INTO content VALUES ('44', '5', '8', '');
INSERT INTO content VALUES ('45', '5', '9', '');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(511) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO user VALUES ('1', 'admin', 'f865b53623b121fd34ee5426c792e5c33af8c227');
