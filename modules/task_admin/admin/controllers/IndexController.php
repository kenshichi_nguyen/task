<?php
class Admin_IndexController extends Base_Controller_Action
{
    public function indexAction()
    {
        $this->_response->setBody($this->view->render($this->_verifyScriptName('index/index.phtml')));
    }

}
