<?php

class Admin_PasswordController extends Base_Controller_Action
{
    public function initOther()
    {
        $this->view->currentMenu = 'password';
    }

    public function indexAction()
    {
        $registry = Zend_Registry::getInstance();
        $messages = array();
        $result = Base_Constant_Client::SUCCESSFUL;
        if ($this->_request->isPost()) {
            $current = $this->_request->getParam('current');
            $new = $this->_request->getParam('new');
            $confirm = $this->_request->getParam('confirm');

            switch (true) {
                case !$current || strlen($current) < Admin_Constant_Server::MIN_PASSWORD_LENGTH:
                    $messages[] = $registry['Zend_Translate']->translate('PLEASE_TYPE_CURRENT_PASSWORD');
                    break;
                case !$new:
                    $messages[] = $registry['Zend_Translate']->translate('PLEASE_TYPE_NEW_PASSWORD');
                    break;
                case $new != $confirm:
                    $messages[] = $registry['Zend_Translate']->translate('NEW_AND_CONFIRM_PASSWORD_NOT_SAME');
                    break;
                case strlen($new) < Admin_Constant_Server::MIN_PASSWORD_LENGTH:
                    $messages[] = $registry['Zend_Translate']->translate('MIN_LENGTH_NEW_PASSWORD_IS');
                    break;
                default:
                    $user = Admin_Model_DbTable_User::getInstance();
                    $currentUser = Zend_Auth::getInstance()->getIdentity();
                    $found = $user->fetchRow(array(
                        'username = ?' => $currentUser->username,
                        'password = ?' => sha1($current)
                    ));
                    if ($found) {
                        $data = array(
                            'password' => sha1($new),
                        );
                        try {
                            $where = $user->getAdapter()->quoteInto('id = ?', $currentUser->id);
                            $user->update($data, $where);
                        } catch (Zend_Db_Exception $dbException) {
                            $result = Base_Constant_Client::FAILED;
                            $message = $dbException->getMessage();
                        }
                        if ($result == Base_Constant_Client::FAILED) {
                            $messages[] = $registry['Zend_Translate']->translate('THERE_ERROR_WHEN_CHANGE_PASSWORD');
                            $messages[] = $message;
                        } else {
                            $messages[] = $registry['Zend_Translate']->translate('CHANGE_PASSWORD_SUCCESSFUL');
                        }

                    } else {
                        $messages[] = $registry['Zend_Translate']->translate('CURRENT_PASSWORD_FAILURE');
                    }
                    break;
            }

        }
        $this->view->messages = Base_Form_Abstract::array2String($messages);
        $this->_response->setBody($this->view->render($this->_verifyScriptName('password.phtml')));
    }

}
