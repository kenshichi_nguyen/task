<?php

class Admin_LoginController extends Base_Controller_Action
{
    public function initOther()
    {
        $this->view->currentMenu = 'login';
    }

    public function indexAction()
    {
        $helper = new Admin_Helper_Facebook();
        $userFb = $helper->getUser();
        $this->view->helper = $helper;
        if ($this->_request->isPost()) {
            $username = $this->_request->getParam('username');
            $password = $this->_request->getParam('password');
            $this->checkUser($username, $password);
        } elseif ($userFb) {
            $username = $userFb['email'] != '' ? $userFb['email'] : $userFb['id'];
            $password = $userFb['id'];
            $this->checkUser($username, $password, 'facebook');
        }
        $this->_response->setBody($this->view->render($this->_verifyScriptName('login.phtml')));
    }

    public function checkUser($username,$password,$flag='normal')
    {
        $user = Admin_Model_DbTable_User::getInstance();
        if ($user->authenticate($username, $password)) {
            Zend_Registry::getInstance()->session->destination->username = $username;
        } elseif ($flag == 'facebook') {
            if($user->addNewUser($username,$password)){
                Zend_Registry::getInstance()->session->destination->username = $username;
            }
        }
        $this->redirect('/' . ADMIN_REWRITE_SOURCE);
    }
    public function logoutAction()
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        Zend_Session::destroy();
        $this->redirect('/' . ADMIN_REWRITE_SOURCE . '/login');
    }

}
