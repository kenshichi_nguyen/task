<?php

class Admin_ContentController extends Base_Controller_Action
{
    public function initOther()
    {
            $this->view->currentMenu = 'cell';
            $registry = Zend_Registry::getInstance();
            $reflection = new ReflectionClass('Admin_Constant_Server');
            $registry['headScript']['inline_' . count($registry['headScript'])] =
                'var Admin_Constant_Server = ' . Zend_Json_Encoder::encode($reflection->getConstants()) . ';';
    }

    public function indexAction()
    {
        if(Zend_Session::namespaceIsset('destination'))
        {
        $this->listAction();
        }else{
            $this->redirect('/' . ADMIN_REWRITE_SOURCE . '/login');
        }
    }

    public function getHtmlList($flag = true, $html = array())
    {
        $table = Admin_Model_DbTable_Content::getInstance();
        $cells = $table->fetchAll();
        $titles = array();
        $tasks = array();
        $tmpArray = array();
        $focuses = array();
        foreach ($cells as $cell) {
            $titles[$cell['row_id']] = $cell['row_id'];
            $tasks[$cell['cell_id']] = $cell['cell_id'];
            $tmpArray[$cell['row_id']][$cell['cell_id']] = $cell['text'];
            $focuses[$cell['row_id']][$cell['cell_id']] = $cell['focus'];
        }
        $cells = $tmpArray;
        $this->view->cells = $cells;
        $this->view->titles = $titles;
        $this->view->tasks = $tasks;
        $this->view->focuses = $focuses;

        //new logt($titles, $tasks, $cells);
        if ($flag) {
            $this->_response->setBody($this->view->render($this->_verifyScriptName('cell/list.phtml')));
        } elseif (!empty($html)) {
            $html['html'] = $this->view->render($this->_verifyScriptName('cell/cell.phtml'));
            $this->_response->setBody($this->_helper->getHelper('json')->encodeJson($html));
        }
    }

    public function listAction()
    {
        $this->getHtmlList();
    }
    public function addAction(){
        $params = $this->_request->getParams();
        $type = (int)$params['type'];
        $this->addMore($type);
    }
    public function addMore($type) //type = 1 : add row; || type = 2 : add cell
    {
        $result = Base_Constant_Client::SUCCESSFUL;
        $message = '';
        $errors = array();
        $table = Admin_Model_DbTable_Content::getInstance();

        $maxRow = $table->fetchAll(
            $table->select()
                ->from($table, array(new Zend_Db_Expr('max(row_id) as maxId')))
        );
        $rowArray = $maxRow->toArray();
        $maxRowId = $rowArray[0]['maxId'];

        if ($maxRowId == null) {
            $maxRowId = 0;
        }
        $maxCell = $table->fetchAll(
            $table->select()
                ->from($table, array(new Zend_Db_Expr('max(cell_id) as maxId')))
        );
        $cellArray = $maxCell->toArray();
        $maxCellId = $cellArray[0]['maxId'];
        if ($maxCellId == null) {
            $maxCellId = 1;
        }

        try {
            if($type==1){
                //Add row
                for ($i = 0; $i < $maxCellId; $i++) {
                    $newRid = $table->insert(array(
                        'id' => '',
                        'row_id' => ((int)$maxRowId + 1),
                        'cell_id' => $i + 1,
                        'text' => '',
                    ));
                }
            }elseif($type==2){
                //Add row
                for ($i = 0; $i < $maxRowId; $i++) {
                    $newRid = $table->insert(array(
                        'id' => '',
                        'cell_id' => ((int)$maxCellId + 1),
                        'row_id' => $i + 1,
                        'text' => '',
                    ));
                }
            }

        } catch (Zend_Db_Exception $dbException) {
            $result = Base_Constant_Client::FAILED;
            $message = $dbException->getMessage();
        }
        $clientData = array(
            'result' => $result,
            'message' => $message,

        );
        $this->getHtmlList(false, $clientData);
    }

    public function updateAction()
    {
        $params = $this->_request->getParams();

        $result = Base_Constant_Client::SUCCESSFUL;
        $message = '';
        $errors = array();
        $table = Admin_Model_DbTable_Content::getInstance();
        $data = array(
            'cell_id' => (int)$params['cell_id'],
            'row_id' => (int)$params['row_id'],
            'text' => $params['text'],

        );
        try {
            $where = array();
            $where[] = $table->getAdapter()->quoteInto('row_id = ?', $params['row_id']);
            $where[] = $table->getAdapter()->quoteInto('cell_id = ?', $params['cell_id']);
            $table->update($data, $where);
        } catch (Zend_Db_Exception $dbException) {
            $result = Base_Constant_Client::FAILED;
            $message = $dbException->getMessage();
        }

        $clientData = array(
            'result' => $result,
            'message' => $message,
            'params' => $params,
        );
        $this->_response->setBody($this->_helper->getHelper('json')->encodeJson($clientData));
    }
    public function emptyAction()
    {
        $params = $this->_request->getParams();
        $type = (int)$params['type'];

        $result = Base_Constant_Client::SUCCESSFUL;
        $message = '';
        $errors = array();
        $table = Admin_Model_DbTable_Content::getInstance();

        try {
            if($type==1){
                $where = $table->getAdapter()->quoteInto('cell_id = ?', $params['id']);
            }elseif($type==2){
                $where = $table->getAdapter()->quoteInto('row_id = ?', $params['id']);
            }

            $table->delete($where);

        } catch (Zend_Db_Exception $dbException) {
            $result = Base_Constant_Client::FAILED;
            $message = $dbException->getMessage();
        }
    }
    public function refreshAction(){
        $table = Admin_Model_DbTable_Content::getInstance();
        $cells = $table->fetchAll();
        $titles = array();
        $tasks = array();
        $tmpArray = array();
        $focuses = array();
        foreach ($cells as $cell) {
            $titles[$cell['row_id']] = $cell['row_id'];
            $tasks[$cell['cell_id']] = $cell['cell_id'];
            $tmpArray[$cell['row_id']][$cell['cell_id']] = $cell['text'];
            $focuses[$cell['row_id']][$cell['cell_id']] = $cell['focus'];
        }
        $cells = $tmpArray;
        $focus = $focuses;
        $respond = array();
        $respond['text'] = $cells;
        $respond['focus'] = $focus;


        $this->_response->setBody($this->_helper->getHelper('json')->encodeJson($respond));
    }
    public function updatefocusAction(){
        $params = $this->_request->getParams();
        $result = Base_Constant_Client::SUCCESSFUL;
        $message = '';
        $errors = array();
        $table = Admin_Model_DbTable_Content::getInstance();
        $data = array(
            'cell_id' => (int)$params['cell_id'],
            'row_id' => (int)$params['row_id'],
            'focus' => (int)$params['focus'],
        );
        try {
            $where = array();
            $where[] = $table->getAdapter()->quoteInto('row_id = ?', $params['row_id']);
            $where[] = $table->getAdapter()->quoteInto('cell_id = ?', $params['cell_id']);
            $table->update($data, $where);
        } catch (Zend_Db_Exception $dbException) {
            $result = Base_Constant_Client::FAILED;
            $message = $dbException->getMessage();
        }
        $clientData = array(
            'result' => $result,
            'message' => $message,
            'params' => $params,
        );
        $this->_response->setBody($this->_helper->getHelper('json')->encodeJson($clientData));
    }
}
