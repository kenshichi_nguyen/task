<?php

class Admin_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $user = Zend_Auth::getInstance()->getIdentity();
        $redirecting = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        switch (true) {
            // not check authentication when the module is not admin and not base
            case 'base' != $request->getModuleName() && ADMIN_REWRITE_MODULE != $request->getModuleName():
                return;
            // logged-in user is redirected to admin home page
            case $user && '/' . ADMIN_REWRITE_TARGET . '/login' == $request->getRequestUri():
                $redirecting->gotoUrl('/' . ADMIN_REWRITE_SOURCE)->redirectAndExit();
            // only login user access logout
            case $user && '/' . ADMIN_REWRITE_TARGET . '/login/logout' == $request->getRequestUri():
                // logged-in user is passed
            case $user:
                // only not-login user access login
            case !$user && '/' . ADMIN_REWRITE_TARGET . '/login' == $request->getRequestUri():
                return;
            default:
                $params = $request->getParams();
                // not be ajax request
                if (!isset($params['ajax']) && !$request->isXmlHttpRequest()) {
                    $origin = $request->getParam('originalRequestUri');
                    Zend_Registry::getInstance()->session->destination->url = $origin ? $origin : $request->getPathInfo();
                }
                unset($params);
                $redirecting->gotoUrl('/' . ADMIN_REWRITE_SOURCE . '/login')->redirectAndExit();
                break;
        }
    }

}