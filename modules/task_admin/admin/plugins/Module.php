<?php

class Admin_Plugin_Module
{
    public static function init()
    {
        $paths = array(
            WWW_PATH . DS . Admin_Constant_Server::MEDIA . DS . Admin_Constant_Server::ORIGINAL_PICTURE_DIRECTORY,
            WWW_PATH . DS . Admin_Constant_Server::MEDIA . DS . Admin_Constant_Server::SMALL_PICTURE_DIRECTORY,
            WWW_PATH . DS . Admin_Constant_Server::MEDIA . DS . Admin_Constant_Server::MEDIUM_PICTURE_DIRECTORY,
            WWW_PATH . DS . Admin_Constant_Server::MEDIA . DS . Admin_Constant_Server::LARGE_PICTURE_DIRECTORY,
        );
        foreach ($paths as $path) {
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
        }
    }

    public static function loadCss()
    {
        return array(
            STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'css' . DS . 'bootstrap' . MIN_VERSION . '.css',
            STATIC_PATH . DS . 'jquery.easyui-1.3.4' . DS . 'easyui.css',
        );
    }

    public static function loadJs()
    {
        return array(
            STATIC_PATH . DS . 'jquery-1.10.2' . MIN_VERSION . '.js',
            STATIC_PATH . DS . 'jquery.form' . MIN_VERSION . '.js',
            STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'js' . DS . 'bootstrap' . MIN_VERSION . '.js',
            STATIC_PATH . DS . 'jquery.easyui-1.3.4' . DS . 'jquery.easyui.min.js',
        );
    }

    public static function loadResource()
    {
        return array(
            STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'css' . DS . 'bootstrap.css.map',
            STATIC_PATH . DS . 'other' . DS . 'image.svg',
            Base_Constant_Server::TARGET => array(
                '../fonts' => array(
                    STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'fonts' . DS . 'glyphicons-halflings-regular.eot',
                    STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'fonts' . DS . 'glyphicons-halflings-regular.svg',
                    STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'fonts' . DS . 'glyphicons-halflings-regular.ttf',
                    STATIC_PATH . DS . 'bootstrap-3.1.1' . DS . 'fonts' . DS . 'glyphicons-halflings-regular.woff',
                ),
                './images' => array(
                    STATIC_PATH . DS . 'jquery.easyui-1.3.4' . DS . 'images' . DS . 'tree_icons.png'
                )
            ),
            STATIC_PATH . DS . 'html5shiv-3.6.2.min.js',
            STATIC_PATH . DS . 'respond-1.3.0.min.js',
        );
    }
}