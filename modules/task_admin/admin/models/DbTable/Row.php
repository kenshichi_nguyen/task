<?php
class Admin_Model_DbTable_Row extends Base_Model_DbTable_Abstract
{

    protected $_name = 'row';
    protected static $_instance = NULL;

    public function __construct($config = array())
    {
        parent::__construct($config);
    }
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

}