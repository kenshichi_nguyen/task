<?php

class Admin_Model_DbTable_User extends Base_Model_DbTable_Backend
{
    protected $_area = self::AREA_BACKEND;
    protected $_name = 'user';
    protected static $_instance = NULL;

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function authenticate($username, $password)
    {
        if (!$username || !$password) {
            return false;
        }
        $auth = Zend_Auth::getInstance();
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->getAdapter());
        $authAdapter->setTableName($this->_name)
            ->setIdentityColumn('username')
            ->setCredentialColumn('password');
        $authAdapter->setIdentity($username);
        $authAdapter->setCredential(sha1($password));
        if ($auth->authenticate($authAdapter)->isValid()) {
            $data = $authAdapter->getResultRowObject(null, array('password'));
            $data->area = $this->_area;
            $auth->getStorage()->write($data);
            return true;
        }
        return false;
    }

    public function addNewUser($username, $password)
    {
        if (!$username || !$password) {
            return false;
        }
        $table = Admin_Model_DbTable_User::getInstance();
        $data = array(
            'username' => $username,
            'password' => sha1($password)
        );
        if ($table->insert($data)) {
            return true;
        }
        return false;
    }
}