/**
 * button state manager
 * state: default, disabled, not clicked, ...
 * noun: buttons, states, button gui
 * verb: action, switch state, change gui
 * each state is copy of origin (default)
 * copy format: ID-suffix
 */
var StateManager = {
    always_create: true,
    all_events: {
        'click': 'onclick',
        'change': 'onchange'
    },
    states: {
        // default
        undefined: {
            suffix: '',  // not suffix
            'class': '', // not new class
            event: undefined // or null
        },
        // all actions is allowable
        all_enable: {
            suffix: '__all_enable',
            'class': 'all_enable on',
            event: undefined // or null
        },
        // all actions is not allowable
        all_disable: {
            suffix: '__all_disable',
            'class': 'all_disable off', // off hover
            event: '' // disable all
        },
        // only click is not allowable
        not_click: {
            suffix: '__not_click',
            'class': 'not_click',
            event: 'click' // split by ','
        }
    },
    init: function () {

    },
    /**
     * switch state
     */
    switchTo: function (id, to_state) {
        var i, j; // browse object array
        var list_event = []; // list events is removed
        var stop = false; // flag to stop
        var create = false; // flag to copy
        var newId; // store new copy id
        var newButton;

        // validate default button whether exist
        $('#' + id).length > 0 ? null : stop = true;
        if (stop) {
            alert('variable not exist');
            return;
        } else {
        }
        // validate state whether exist
        this.states[to_state] != undefined ? null : stop = true;
        if (stop) {
            alert('state not exist');
            return;
        } else {
        }
        // validate copy id
        newId = id + this.states[to_state]['suffix'];

        if (newId != id && this.always_create == true) {
            $('#' + newId).remove();
        } else {
        }
        // validate copy whether exist
        $('#' + newId).length > 0 ? null : create = true;
        // create copy if not exist
        if (create) {
            newButton = $('#' + id).clone().attr('id', newId);
            newButton = newButton[0];
            list_event = this.states[to_state]['event'];
            if (list_event != undefined) {
                list_event = list_event.split(',');
                // remove all event handler
                if (list_event.length == 1 && list_event[0] == '') {
                    $(newButton).unbind();
                    $(newButton).die();
                    for (j in this.all_events) {
                        eval("$(newButton)[0]." + this.all_events[j] + " = null;");
                        eval("$(newButton).attr('" + this.all_events[j] + "', null);");
                    }
                } else {
                    for (i in list_event) {
                        if (list_event[i] != '') {
                            $(newButton).unbind(list_event[i]); // remove event
                            $(newButton).die(list_event[i]);
                            eval("$(newButton)[0].on" + list_event[i] + " = null;"); // plain js
                            eval("$(newButton).attr('on" + list_event[i] + "', null);"); // jquery
                        } else {
                        }
                    }
                }
            }
            for (i in this.states) {
                $(newButton).removeClass(this.states[i]['class']);
            }
            $(newButton).addClass(this.states[to_state]['class']);

            $(newButton).insertAfter($('#' + id));
        }
        // hide all default and copies
        $('[id^=' + id + '__]').hide();
        $('#' + id).hide();

        // show a copy
        $('#' + newId).show();
    }
}
StateManager.init();
// add as jquery plugin
(function ($) {
    $.fn.switchStateTo = function (state) {
        typeof state != 'string' && typeof state != 'undefined' ? state = '' : null;
        if (state == '') {
            return;
        } else {
        }

        return this.each(function () {
            StateManager.switchTo(this.id, state);
        });
    };
})(jQuery);