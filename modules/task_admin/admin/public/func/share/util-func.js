/**
 * @for system, include global function
 */

/**
 * global tmp variable
 */
$.tmp = null;

/**
 * log
 */
$.log = function (param) {
    try {
        // @link http://stackoverflow.com/questions/591857/how-can-i-get-a-javascript-stack-trace-when-i-throw-an-exception
        console.log('');
        console.log('# The stacktrace exception is:');
        console.log(param.stack || e.stacktrace);
        console.log('# The caller (throws the exception) is:');
        console.log(arguments.callee.caller.name || '(anonymous)', ' = ', arguments.callee.caller);
        console.log('# The arguments is:');
        console.log(arguments);
    }
    catch (e) {

    }
}
/**
 * global registry
 */
$.registry = {
    /**
     * check key whether exist or not
     * if not exist, init
     * @param varName
     * @param dataType
     * @returns {boolean}
     */
    check: function (varName, dataType) {
        try {
            var strValue;
            var defValue;

            // validate parameters
            if ('string' != $.type(varName) || '' == varName) {
                return false;
            }
            if ('string' != $.type(dataType) || '' == dataType) {
                return false;
            }
            $.tmp = null;
            $.globalEval('$.tmp = $.registry.' + varName + ';');
            if (!$.tmp) {
                defValue = {
                    _undefined: 'undefined',
                    _null: 'null',
                    _boolean: 'false',
                    _number: '0',
                    _string: '',
                    _function: 'function () {}',
                    _array: '[]',
                    _date: 'new Date()',
                    _regexp: '//',
                    _object: '{}'
                };
                strValue = defValue[ '_' + dataType ];
                if (null === strValue || undefined === strValue) {
                    return false;
                }
                $.globalEval('$.registry.' + varName + ' = ' + strValue + ';');
            }
        }
        catch (e) {
            $.log(e);
            return false;
        }
        return true;
    }
}

/**
 * auto grow text area
 * global variable $.registry.txtaInitHeights[txtaId] = -1;
 * @param txtaNode HTMLTextAreaElement
 * @event focus, input, keydown, paste
 */
$.autogrow = function (txtaNode) {

    // get text area element
    var txtaMetrics;
    var txtaWidth;
    var txtaId;
    var tmp;
    var borderBoxOffset;
    var isBorderBox;
    $.registry.check('txtaCount', 'number');
    $.registry.check('txtaInitHeights', 'object');

    // validate element node type
    if (
        'object' != $.type(txtaNode) // not object
            || null == $(txtaNode).prop('tagName') // not HtmlElement
            || 'TEXTAREA' != $(txtaNode).prop('tagName').toUpperCase() // not HTMLTextAreaElement
        ) {
        return;
    }
    // id is not set
    if (!( txtaId = $(txtaNode).attr('id') )) {
        $.registry.txtaCount++;
        while (txtaId = 'textarea' + $.registry.txtaCount) {
            if (0 == $('#' + txtaId).length) {
                break;
            }
            $.registry.txtaCount++;
        }
        $(txtaNode).attr('id', txtaId);
    }
    // create text area metrics
    txtaMetrics = $('<textarea id="textMetrics' + txtaId + '" class="textMetrics"></textarea>').appendTo('body') [ 0 ];

    $(txtaMetrics).css('fontSize', $(txtaNode).css('fontSize'));
    $(txtaMetrics).css('fontStyle', $(txtaNode).css('fontStyle'));
    $(txtaMetrics).css('fontWeight', $(txtaNode).css('fontWeight'));
    $(txtaMetrics).css('fontFamily', $(txtaNode).css('fontFamily'));
    $(txtaMetrics).css('wordWrap', $(txtaNode).css('wordWrap'));
    tmp = $(txtaNode).css('lineHeight');
    !!$.browser.webkit ? tmp = Math.round(parseInt(tmp, 10) * 1) + 'px' : null;
    $(txtaMetrics).css('lineHeight', tmp);

    borderBoxOffset = parseFloat($(txtaNode).css('padding-top'), 10)
        + parseFloat($(txtaNode).css('padding-bottom'), 10)
        + parseFloat($(txtaNode).css('border-top-width'), 10)
        + parseFloat($(txtaNode).css('border-bottom-width'), 10);

    // init global var $.registry.txtaInitHeights[txtaId] = -1;
    // init height
    if (!$.registry.txtaInitHeights [ txtaId ]) {
        $.registry.txtaInitHeights [ txtaId ] = txtaNode.offsetHeight - borderBoxOffset;
    }
    txtaWidth = $(txtaNode).prop('clientWidth')
        - parseFloat($(txtaNode).css('padding-left'), 10)
        - parseFloat($(txtaNode).css('padding-right'), 10);
    // set metrics width
    $(txtaMetrics).css('width', Math.max(txtaWidth, 0) + 'px');
    // insert content for metrics
    $(txtaMetrics).append(
        document.createDocumentFragment().appendChild(
            document.createTextNode(
                $(txtaNode).val() + '...'
            )
        )
    );
    tmp = Math.max($.registry.txtaInitHeights[txtaId], txtaMetrics.scrollHeight);
    isBorderBox = 'border-box' === $(txtaNode).css('box-sizing')
        || 'border-box' === $(txtaNode).css('-moz-box-sizing')
        || 'border-box' === $(txtaNode).css('-webkit-box-sizing');

    isBorderBox ? tmp += borderBoxOffset : 0;
    // set new height
    $(txtaNode).css('height', tmp + 'px');
    $(txtaMetrics).remove();
}

$.replaceHtml = function (oldElement, html) {
    try {
        'string' == $.type(oldElement) ? oldElement = $('#' + oldElement)[0] : oldElement = $(oldElement)[0];
    }
    catch (e) {
        return null;
    }
    // Pure innerHTML is slightly faster in IE
    /*@cc_on
     oldElement.innerHTML = html;
     return oldElement;
     @*/
    var newElement = oldElement.cloneNode(false);
    newElement.innerHTML = html;
    oldElement.parentNode.replaceChild(newElement, oldElement);
    /*
     Since we just removed the old element from the DOM, return a reference
     to the new element, which can be used to restore variable references.
     */
    return newElement;
};