if ('undefined' == $.type(ModuleFunction)) {
    var ModuleFunction = {};
}
if (!$.isPlainObject(ModuleFunction)) {
    ModuleFunction = {};
}
ModuleFunction = {
    init: function () {
        //make size of table cell
        var wd = $(window).width();
        var cols = $("table > tbody").find("> tr:first > td").length;
        $("#cellcontainer").css('width', ((32.65 * wd) / 100) * cols);
        $("tr td").css('width', (32.65 * wd) / 100);
        // change color of text
        $("textarea").change(function () {
            $(this).parent().attr('class', 'focus');
            $(this).parent().css('border', '1px double purple');
        });

        $('td').click(function(){
            $(this).find('textarea').focus();
        });

        //auto size autosize
        $('textarea').autosize();
        //auto size focusEvent
        ModuleFunction.focusEvent();
    },

    focusEvent: function () { //Focus event

        var save_interval;
        $('textarea').focus(function () {

            var beforeText = $(this).val();
            $(this).parent().attr('class', 'focus');
            var name = $(this).attr('id');

            $(this).change(function () {
                $(this).attr('class', 'changed');
                $(this).parent().css('border', '1px double purple');
                //disable typing
                if(name != '' && name != null) {
                    if(name.indexOf("_") != -1){
                        name = name.split("_")[1];
                    }
                    var c = name.split("c")[1];
                    var r = name.split("c")[0].split("r")[1];
                    ContentController.updateFocus(r, c, 1);
                }
            });

            save_interval = setInterval(
                function () {
                    if ($('#' + name).val() != beforeText) {
                        ModuleFunction.save(name);
                        beforeText = $('#' + name).val();
                    }
                }, 10000);
        });
        $('textarea').focusout(function () {
            $(this).parent().removeClass("focus");
            clearInterval(save_interval);
            //enable typing
            var name = $(this).attr('id');
            if(name != '' && name != null) {
                if(name.indexOf("_") != -1){
                    name = name.split("_")[1];
                }
                var c = name.split("c")[1];
                var r = name.split("c")[0].split("r")[1];
                //change focus status
                ContentController.updateFocus(r, c, 0);
                $(this).parent().removeAttr("disabled");
            }
        });
    },

    save: function (name) {  //save content function
        if(name != '' && name != null) {
            if(name.indexOf("_") != -1){
                name = name.split("_")[1];
            }
            var c = name.split("c")[1];
            var r = name.split("c")[0].split("r")[1];
            ContentController.autoUpdate(r, c);
        }
    },

    changestatus: function () { //change color when update success
        $("textarea.changed").each(function () {
            $(this).attr('class', 'success');
        });
        setTimeout(function () {
            $("textarea.success").each(function () {
                $(this).removeClass('success');
                $(this).parent().css('border', '1px solid #2F96B4');
            });
        }, 2000);
    },

    addCell: function () { //add 1 row to table cell
        $("#cellcontainer").each(function (index, element) {
            var tds = '<tr>';
            var cell = $("table > tbody").find("> tr").length + 1;
            jQuery.each($('tr:last td', this), function (index1, element1) {
                tds += '<td><textarea id=' + '"text_r' + (index1 + 1) + 'c' + cell + '"' + ' onchange="ContentController.updateInstant(' + (index1 + 1) + ',' + cell + ');"></textarea></td>';
            });
            tds += '</tr>';
            if ($('#cellcontainer tr').length > 0) {
                $('tbody', this).append(tds);
                $('#text_r1c' + cell).focus();
                ContentController.add(2);
            } else {
                tds = '<tr><td><textarea id="text_r1c1" onchange="ContentController.updateInstant(1,1);"></textarea></td></tr>';
                $('#cellcontainer').append(tds);
                $('#text_r1c1').focus();
                ContentController.add(1);
            }
        });
        ModuleFunction.init();
    },

    addRow: function () { //add 1 column to table cell
        var row = $("table > tbody").find("> tr:first > td").length + 1;
        $('#cellcontainer').find('tr').each(function (index, element) {
            var tds = '<td><textarea id=' + '"text_r' + row + 'c' + (index + 1) + '"' + ' onchange="ContentController.updateInstant(' + row + ',' + (index + 1) + ');"></textarea></td>';
            $(this).append(tds);
            $('#text_r' + row + 'c1').focus();
        });
        if ($('#cellcontainer tr').length == 0) {
            var tds = '<tr><td><textarea id="text_r1c1" onchange="ContentController.updateInstant(1,1);"></textarea></td></tr>';
            $('#cellcontainer').append(tds);
            $('#text_r1c1').focus();
        }
        ContentController.add(1);
        ModuleFunction.init();
    },

    focusTimeout: function (id) {
        var timeout;
        $(id).keypress(function() {
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                ModuleFunction.removeDisable(id);
            }, 10000);
        });
        timeout = setTimeout(function(){
            ModuleFunction.removeDisable(id);
        }, 10000);
    },

    removeDisable: function (id) {
        $(id).removeAttr("disabled");
        var name = $(id).attr('id');
        if(name != '' && name != null) {
            if(name.indexOf("_") != -1){
                name = name.split("_")[1];
            }
            var c = name.split("c")[1];
            var r = name.split("c")[0].split("r")[1];
            //change focus status
            ContentController.updateFocus(r, c, 0);
        }
    },

    empty: function (type) { // delete blank row / blank column
        var cell = $('table > tbody').find('> tr').length;
        var row = $('table > tbody').find('> tr:first > td').length;
        if (type == 1) {   //delete cell
            for (var i = 1; i <= cell; i++) {
                var text1 = '';
                for (var j = 1; j <= row; j++) {
                    text1 += $('#text_r' + j + 'c' + i).val();
                }
                if (text1.trim() == '') {
                    var k = 1;
                    for (k = 1; k <= row; k++) {
                        for (var l = i; l < cell; l++) {
                            $('#text_r' + k + 'c' + l).val($('#text_r' + k + 'c' + (l + 1)).val());
                            ContentController.updateInstant(k, l);
                        }
                        if (k == row) {
                            $('#text_r1c' + cell).parent().parent().remove();
                        }
                        $('#text_r' + k + 'c' + cell).val('');
                    }
                    ContentController.empty(type, cell);
                    ModuleFunction.empty(type);
                }
            }
        } else if (type == 2) { //delete col
            for (var r = 1; r <= row; r++) {
                var text2 = '';
                for (var c = 1; c <= cell; c++) {
                    text2 += $('#text_r' + r + 'c' + c).val();
                }
                if (text2.trim() == '') {
                    for (var k = 1; k <= cell; k++) {
                        for (var l = r; l < row; l++) {
                            $('#text_r' + l + 'c' + k).val($('#text_r' + (l + 1) + 'c' + k).val());
                            ContentController.updateInstant(l, k);
                        }
                        if (k == cell) {
                            ContentController.updateInstant(row - 1, k);
                        }
                        $('#text_r' + row + 'c' + k).val('');
                        $('#text_r' + row + 'c' + k).parent().remove();
                    }
                    ContentController.empty(type, row);
                    ModuleFunction.empty(type);
                }
            }
        } else {
            console.log('unknown');
        }
        // focus to top
        $('#text_r1c1').focus();
    },

    getTimeNow: function () {  // function time
        var d = new Date();
        var s = d.getSeconds();
        var m = d.getMinutes();
        var h = d.getHours();
        var day = d.getDay();
        var date = d.getDate();
        var month = d.getMonth();
        var year = d.getFullYear();
        var days = new Array("Chủ nhật", "Thứ hai", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7");
        var months = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
        if (s < 10) {
            s = "0" + s
        }
        if (m < 10) {
            m = "0" + m
        }
        if (h > 12) {
            h -= 12;
            AM_PM = "PM"
        }
        else {
            AM_PM = "AM"
        }
        if (h < 10) {
            h = "0" + h
        }
        return days[day] + ", " + date + "/" + months[month] + "/" + year + " - " + h + ":" + m + ":" + s + " " + AM_PM;
        setTimeout(ModuleFunction.getTimeNow, 1000);
    },

    refreshContent: function (id, respond) { //auto refresh content, sync with server every 10s
        var cell = $('table > tbody').find('> tr').length;
        var row = $('table > tbody').find('> tr:first > td').length;
        if ($(".focus").children().attr("id")) { //get id of textarea is focusing
            var name = $(".focus").children().attr("id");
            if(name != '' && name != null) {
                if(name.indexOf("_") != -1){
                    name = name.split("_")[1];
                }
                var c = name.split("c")[1];
                var r = name.split("c")[0].split("r")[1];
            }
        }
        for (var i = 1; i <= row; i++) {
            for (var j = 1; j <= cell; j++) {
                if (name) {
                    if (i != r || j != c) { //in case user focusing, only apply for exception cells
                        $("#"+name).val(respond.text[i][j]);
                        if(respond.focus[i][j]==1){
                            $("#"+name).attr("disabled", "disabled");
                            ModuleFunction.focusTimeout("#"+name);
                        }else{
                            $("#"+name).removeAttr("disabled");
                        }
                    }
                } else { //user not focus
                    $('#text_r' + i + 'c' + j).val(respond.text[i][j]);
                    if(respond.focus[i][j]==1){
                        $('#text_r' + i + 'c' + j).attr("disabled", "disabled");
                        ModuleFunction.focusTimeout('#text_r' + i + 'c' + j);
                    }else{
                        $('#text_r' + i + 'c' + j).removeAttr("disabled");
                    }
                }
            }
        }
    }
}