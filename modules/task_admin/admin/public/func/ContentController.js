if ('undefined' == $.type(ContentController)) {
    var ContentController = {};
}
if (!$.isPlainObject(ContentController)) {
    ContentController = {};
}
ContentController = {
    updateInstant: function (row,cell) {
        var serverData = {
            row_id: row,
            cell_id: cell,
            text: $('#text_r'+row+'c'+cell).val()
        };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/update?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else {
                    ModuleFunction.changestatus();
                }
            }
        });
    },
    autoUpdate: function (row,cell) {
        var serverData = {
            row_id: row,
            cell_id: cell,
            text: $('#text_r'+row+'c'+cell).val()
        };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/update?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else{
                    //notice when post success
                }
            }
        });
    },
    add: function (type) {
        var serverData = {
            type: type
        };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/add?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else {
                    ModuleFunction.changestatus();
                }
            }
        });
    },
    empty: function (type,id) {
        var serverData = {
                type: type,
                id: id
            };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/empty?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else {
                    ModuleFunction.changestatus();
                }
            }
        });
    },
    refresh: function () {
        var serverData = {
            idFocus: ''
        };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/refresh?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    //there 2 arrays : responseObj.text and responseObj.focus;
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else {
                    ModuleFunction.refreshContent(null,responseObj);
                }
            }
        });
    },
    updateFocus: function (row,cell,status) {
        var serverData = {
            row_id: row,
            cell_id: cell,
            focus: status
        };
        $.ajax({
            type: 'post',
            url: SERVER.BASE_URL + '/' + SERVER.ADMIN_REWRITE_SOURCE + '/content/updatefocus?ajax',
            data: serverData,
            success: function (data, textStatus, jqXHR) {
                try {
                    var responseObj = $.parseJSON(data);
                }
                catch (e) {
                    return;
                }
                if (Base_Constant_Client.FAILED == responseObj.result) {
                    responseObj.message && alert(responseObj.message);
                    return;
                }
                else{
                    //notice when post success
                }
            }
        });
    }
}