var runningCallback = false;

$(document).ready(function () {
    //function construct
    ModuleFunction.init();

    //change color when update success
    ModuleFunction.changestatus();

    //Show time in page
    $("p.time").append(ModuleFunction.getTimeNow());

    //auto refresh content every 3s
    setInterval(
        ContentController.refresh, 2000
    );

    //style scroll for button right
    var leftOffset = parseInt($("#header").css('left'));
    $(window).scroll(function(){
        $('#header').css({
            'left': $(this).scrollLeft() + leftOffset
        });
    });

});













