<?php
require YOURS_PATH . '/modules/task_admin/admin/plugins/facebook/facebook.php';

class Admin_Helper_Facebook
{

    /**
     * @param array $checked , list category is checked
     * @return array
     */
    protected $_urlLogin;
    protected $_appId = '668574289923016';
    protected $_secret = 'e9048b1c8e4c81b8ce9161d9b51cd84c';

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->_appId;
    }

    /**
     * @param mixed $appId
     */
    public function setAppId($appId)
    {
        $this->_appId = $appId;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->_secret;
    }

    /**
     * @param mixed $secret
     */
    public function setSecret($secret)
    {
        $this->_secret = $secret;
    }

    /**
     * @return mixed
     */
    public function getUrlLogin()
    {
        return $this->_urlLogin;
    }

    /**
     * @param mixed $urlLogin
     */
    public function setUrlLogin($urlLogin)
    {
        $this->_urlLogin = $urlLogin;
    }

    public function getUser()
    {
        $facebook = new Facebook(array(
            'appId' => $this->getAppId(),
            'secret' => $this->getSecret(),
        ));
        $array = array(
            'scope' => 'publish_actions,email',
            'url_login' => BASE_URL
        );
        $this->setUrlLogin($facebook->getLoginUrl($array));
        // Get User ID
        $user = $facebook->getUser();
        if ($user) {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                return $facebook->api('/me');
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
    }
}
