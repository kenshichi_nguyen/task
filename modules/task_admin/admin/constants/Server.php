<?php

class Admin_Constant_Server extends Base_Php_Overloader
{

    private static $_instance = NULL;

    /**
     * Singleton instance
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    const MEDIA = PROJECT_MEDIA;
    const ORIGINAL_PICTURE_DIRECTORY = 'p0x'; // origin directory
    const SMALL_PICTURE_DIRECTORY = 'p123x';
    const MEDIUM_PICTURE_DIRECTORY = 'p303x';
    const LARGE_PICTURE_DIRECTORY = 'p490x';
    const SMALL_PICTURE_SIZE = 123; // size in px
    const MEDIUM_PICTURE_SIZE = 303;
    const LARGE_PICTURE_SIZE = 490;
    const MIN_IMAGE_SIZE = 1000;
    // php.ini
    // memory_limit = 512M
    // upload_max_filesize = 10M
    // post_max_size = 100M
    // file_uploads = On
    const MAX_IMAGE_SIZE = 10000000;
    static $allows = array('jpg', 'jpeg', 'png');
    static $cellStatus = array(0 => 'Không bán', 1 => 'Đang bán');
    static $orderStatus = array(0 => 'Chưa xử lý', 1 => 'Đang xử lý', 2 => "Hoàn thành" , 3 => 'Đã hủy');
    const MIN_PASSWORD_LENGTH = 6;
}